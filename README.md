# YoutubeDownloader
Um script que baixa vídeos do Youtube na maior resolução possível.

## Instalação :rocket::
Para instalar este programa no Linux, você precisa ter o Python e o Pip3:
``` shell
pip3 install PySimpleGUI pytube validators
```
Você também precisa ter o youtube-dl instalado pelo gerenciador de pacotes.

---
Keep it Sweet & Simple
