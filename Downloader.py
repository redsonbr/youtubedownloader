#!/bin/python
# coding=<UTF-8>
import PySimpleGUI as sg
import os
from pytube import YouTube as yt
from validators import url as validateurl
sg.theme('Reddit')
layout = [
    [sg.Text('Digite o Url a ser baixado:'), sg.InputText(key='url')],
    [sg.Text("Deseja baixar o vídeo(com som), ou apenas o aúdio?")],
    [sg.Checkbox('Vídeo',key='videocheck'), sg.Checkbox('Aúdio',key='audiocheck')],
    [sg.Button('Baixar'),sg.Button('Cancelar')]
]

window = sg.Window('Youtube Downloader', layout)

while True:
    evento,valor = window.read()
    valid=validateurl(valor['url'])
    if evento == 'Cancelar' or evento == sg.WIN_CLOSED:
        break
    if evento == 'Baixar' and valor['url'] == '':
        sg.Popup("Você precisa por um URL")
        if evento == 'Baixar' and valor['videocheck'] == False and valor['audiocheck'] == False:
            sg.Popup('Você deve marcar alguma opção? vídeo ou aúdio')
        if evento == 'Baixar' and valor['videocheck'] == True and valor['audiocheck'] == True:
            sg.Popup('Marque apenas uma opção')
    if evento == 'Baixar' and valor['url'] != '' and valor['videocheck'] == True:
        try:
            video = yt(valor['url'])
            title = video.title
            os.system("youtube-dl -o '{}'.'%(ext)s' video.mp4 -f mp4 {}".format(valor['url']))
            confirmacao = os.path.exists('video.mp4')
            if confirmacao == True:
                sg.Popup('O vídeo foi baixado na pasta do Youtube Video Downloader.')
            else:
                sg.Popup('Não foi possível salvar o vídeo')
        except Exception as e:
            raise e
            sg.Popup("Ocorreu um erro com o link ou com o arquivo.")
    if evento == 'Baixar' and valor['url'] != '' and valor['audiocheck'] == True:
        try:
            video = yt(valor['url'])
            title = video.title
            os.system("youtube-dl -o '{}'.'%(ext)s' --audio-format mp3 -x {}".format(title, valor['url']))
            confirmacao = os.path.exists('{}.mp3'.format(title))
            if confirmacao == True:
                sg.Popup('O vídeo foi baixado na pasta do Youtube Video Downloader.')
            else:
                sg.Popup('Não foi possível salvar o vídeo')
        except Exception as e:
            raise e
            sg.Popup("Ocorreu um erro com o link ou com o arquivo.")
    if evento == 'Baixar' and valid!=True:
        sg.Popup("Informe um URL válido!")
